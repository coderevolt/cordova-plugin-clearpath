Pod::Spec.new do |s|
  s.name             = "Clearpath"
  s.version          = "0.1.0"
  s.summary          = "The custom view controller link."
  s.homepage         = "https://bitbucket.org/coderevolt/cordova-plugin-clearpath"
  s.license          = 'MIT.'
  s.author           = { "CodeRevolt" => "srdja.v@coderevolt.com" }
  s.source           = { :git => "https://coderevolt@bitbucket.org/coderevolt/cordova-plugin-clearpath.git", :tag => s.version }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files  = "src/ios/*.{h,m}"
  s.preserve_paths = "*.md","www/*.js"
  s.dependency "Cordova", ">= 4.3.0"

  s.frameworks = 'UIKit'
  s.module_name = 'ClearpathPlugin'
end
