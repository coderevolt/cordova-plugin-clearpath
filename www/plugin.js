
var exec = require('cordova/exec');

var PLUGIN_NAME = 'ClearPathPlugin';

var ClearPathPlugin = {
  callNativeFunction: function(cb, error, phrase) {
    exec(cb, null, PLUGIN_NAME, 'showController', [phrase]);
  }
};

module.exports = ClearPathPlugin;
