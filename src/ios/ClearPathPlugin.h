//
//  ClearPathPlugin.h
//  MDS
//
//  Created by Fearless Mac on 2013-01-12.
//
//

#import <Cordova/CDVPlugin.h>

@interface ClearPathPlugin : CDVPlugin

- (void) showController:(CDVInvokedUrlCommand *)command;

@end
