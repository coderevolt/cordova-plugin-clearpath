//
//  ClearPathPlugin.m
//  MDS
//
//  Created by Code Revolt on 2016-12-12.
//
//

#import "ClearPathPlugin.h"

@implementation ClearPathPlugin

- (void) showController:(CDVInvokedUrlCommand *)command {
    
    NSLog(@"Hello, this is a native function called from PhoneGap/Cordova!");
    
    NSString *className = [command.arguments objectAtIndex:1];
    Class viewClass = NSClassFromString(className);
    UIViewController *viewc = [[viewClass alloc] init];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:viewc animated: YES completion:nil];
    
/*    CDVPluginResult *result;
    
    if ( [resultType isEqualToString:@"success"] ) {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: @"Success :)"];
        [self writeJavascript:[result toSuccessCallbackString:callbackId]];
    }
    else {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString: @"Error :("];
        [self writeJavascript:[result toErrorCallbackString:callbackId]];
    }*/
}

@end
